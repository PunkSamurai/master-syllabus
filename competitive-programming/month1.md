## Month 1: Beginner Level

### **Week 1**

#### Programming Language:
* Choose a beginner-friendly language like Python, C++, or Java. Master syntax, data types, control flow, functions, and input/output operations.
* Practice by solving simple Codeforces problems tagged implementation.
* C++ is the recommended programming language and is the most widely used for CP.

#### [Implementation](https://codeforces.com/problemset?order=BY_SOLVED_DESC&tags=implementation):
* Gain comfort with translating problem statements into code. Practice reading input, processing data, implementing logic, and generating output based on requirements.
* Work on Codeforces problems tagged implementation, particularly A level.


### **Week 2**

#### [Mathematics](https://codeforces.com/problemset?order=BY_SOLVED_DESC&tags=math): 
* Brush up on essential math concepts like number theory (divisors, modulo operations, prime factorization), probability, basic equations, and inequalities.
* Solve Codeforces problems tagged math, starting with A level.
* **NOTE : The importance of mathematics can be understood with a simple example, suppose we are required to find sum of all factors of 3 within a range, instead of the naive approach of looping one can use the concept of ap to compute it within a single step**

#### [Strings](https://codeforces.com/problemset?order=BY_SOLVED_DESC&tags=strings):
* Understand various string operations like concatenation, slicing, searching, manipulation.
* Master the basic string-based algorithms like palindrome check, longest common substring, string matching.
* Solve Codeforces problems tagged string, starting with A levels.


#### Note
Read Chapters 1 and 2 from the CP handbook. This will help you learn C++ fundamentals if you are new to programming. It also introduces some neat tricks in Chapter 1 on how to handle certain inputs, use macros, and employ other techniques to optimize our program.

In Chapter 2, we explore the need for faster algorithms. The maximum subarray example showcases three different algorithms, each demonstrating ways to improve efficiency. You don't need to fully understand the workings of the maximum subarray now; we will cover that soon.


### **Week 3-4**
Over the next two weeks, we will delve into common practices applicable to every problem. For each problem, creating an appropriate data structure is often necessary, and each has its advantages. Therefore, it is essential to learn about each of them to progress. Additionally, we will explore various sorting and searching techniques, which are frequently employed in problem-solving.
#### [Data Structures Basics](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=data+structures):

* Introduction to basic data structures such as arrays, linked lists, stacks, and queues. Understand their properties and basic operations.
* Practice implementing these data structures in your chosen programming language.
* Solve Codeforces problems tagged with data structures, starting with A levels.
#### [Sorting and Searching](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=sortings):

* Explore various sorting algorithms (e.g., bubble sort, insertion sort, merge sort) and understand their time complexity.
* Learn and practice searching algorithms (e.g., binary search) and their implementation.
* Solve Codeforces problems tagged with sorting and searching, starting with A levels.

## 1 Month outcome

Gain a basic understanding of what competitive programming is and how you can get better at it. Also get a better idea of how to approach a problem and create a good solution.

[Back to roadmap](./README.md)