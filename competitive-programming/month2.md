## Month 2: Intermediate Level

### **Week 5-6**
Moving onto some of the more advanced topics, Understanding these concepts is not only essential for interviews but also pivotal in competitive programming. Two fundamental techniques, Greedy Algorithms and Dynamic Programming (DP), become indispensable tools in our problem-solving arsenal.

#### [Greedy Algorithms](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=greedy):

* Explore the concept of greedy algorithms and their application in solving optimization problems.
* Understand the greedy-choice property and the optimal substructure for greedy algorithms.
* Solve Codeforces problems tagged with greedy algorithms, starting with A levels.

#### [Dynamic Programming](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=dp):

* Introduction to dynamic programming concepts. Understand the basic principles of overlapping subproblems and optimal substructure.
* Solve simple dynamic programming problems to grasp the underlying principles.
* Progress to more challenging Codeforces problems tagged with dynamic programming, starting with A levels.

### **Week 7-8**

The most effective way to enhance your CP skills is through continuous practice. Utilize this time to solve a variety of problems on Codeforces, participate in a couple of competitions to familiarize yourself with the format, and solidify your understanding. With the topics covered thus far, you are well-prepared to achieve a rating close to 1000. Maintain a consistent practice routine and revisit the methods learned during this period.

Once you have achieved a rating of 900 or above, it's time to tackle B-type problems primarily. Additionally, occasionally challenge yourself with C-type problems to broaden your problem-solving skills.

#### Task
* Participate in 2 Codeforces contests (preferably Div 4 as it is beginner-friendly).
* Finish reading the first 10 chapters of the CP handbook and familiarize yourself with different problem types.



## 2 Month outcome

A 1000+ rating signifies intermediate proficiency in competitive programming. Embrace B-type problems for complexity and occasional C-type challenges for advanced problem-solving. Deepen your understanding of algorithms and structures, fostering continuous improvement through consistent practice and contest participation.

## Whats next?
Introducing [A2OJ](https://earthshakira.github.io/a2oj-clientside/server/Ladders.html) Ladder, a structured path designed to enhance your competitive programming skills systematically. The ladder is a curated collection of problems categorized by difficulty, starting from the basics and progressing to advanced levels. It provides a clear roadmap for your improvement journey in the competitive programming realm.


Challenge yourself to solve as many problems as you can from the first A2OJ ladder. Each problem you solve is a big step forward in your learning journey. These basic challenges are like building blocks, giving you a solid foundation.

[Back to roadmap](./README.md)