## Month 3: Advanced Level

### **Week 9**
#### [Graphs](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=graphs):

* Understand the fundamentals of graphs, covering nodes, edges, and their relationships.
* Delve into DFS and BFS for traversing or searching tree/graph data structures.
* Read Chapter 11 and 12 of the CP handbook
* Code DFS and BFS in your chosen programming language on simple graph problems.
* Solve A and B level problems tagged with graphs, BFS, and DFS.

End Week 9 with a solid grasp of basic graph concepts, DFS, and BFS algorithms, preparing for more advanced graph-related challenges ahead.

Once you've reached the 1000 rating milestone, it's time to delve into more intricate topics, particularly exploring advanced data structures like graphs. Additionally, challenge yourself with a broader range of B and C type problems to further enhance your problem-solving skills.

### **Week 10**
#### [Shortest path and related algorithms](https://codeforces.com/problemset?order=BY_RATING_ASC&tags=shortest+paths):

* Explore Dijkstra's and Bellman-Ford algorithms for finding the shortest paths in weighted graphs.
* Learn about algorithms like Kruskal's and Prim's for constructing the minimum spanning tree.
* Code these algorithms to reinforce your understanding.
* Solve A and B level problems tagged with shortest paths and MST.

By the end of this week, you'll master essential algorithms for finding efficient paths and constructing minimum spanning trees.

Graph algorithms present a unique challenge, demanding continuous practice to grasp their nuances. During these two weeks, immerse yourself in problem-solving on Codeforces, particularly focusing on graph-related challenges. Engage in contests to assess your progress and solidify your understanding of graph algorithms. With a rating above 900, prioritize B-type problems while occasionally exploring C-type challenges. This targeted approach ensures steady improvement in graph problem-solving skills, propelling you further in the competitive programming realm. Stay dedicated to your practice routine!


### **Week 11-12**
Graph algorithms are usually quite tricky and require constant practice to master. During these two weeks, immerse yourself in problem-solving on Codeforces, particularly focusing on graph-related challenges. prioritize B-type problems while occasionally exploring C-type challenges. This targeted approach ensures steady improvement in graph problem-solving skills, propelling you further in the competitive programming realm.

#### Task
* Participate in 2 Codeforces contests (Aim to solve more than what was previously solved)
* Finish reading the next 10 chapters of the CP handbook and familiarize yourself with different problem types.

## 3 Month outcome

Having covered the basic important topice, you now have the tools to reach 1200 rating or green in codeforces, the key to improvement now lies in practice and a solid grasp of what you've learned. Keep practicing, apply your knowledge effectively, and challenge yourself with new problems.

[Back to roadmap](./README.md)