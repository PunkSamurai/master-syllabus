# Introduction to Competitve Programming
#### By David Veliath and Vyshnav Unnikrishnan

## Introduction

Competitive Programming is a test of a programmer's problem solving ability as well as coding and DSA knowledge. There are many types of problems and many ways to solve each one. A good competitive programmer must be able to quickly grasp the question and find out the most efficient solution as fast as possible.

This guide aims to explain various aspects of Competitive Programming, teach you how to improve your skills, and point you towards helpful platforms and resources that can get you closer to ICPC.

However, nothing good comes easy. Mastering competitive programming is going to take a lot of time and effort. But if you're able to stick with it, or at least work through this guide alongside of whatever domain you are studying, you will see the benefits.

## Prerequisites
Must be comfortable with any programming language, preferably C++ (this is the only prerequisite, we will learn everything else along the way.)

#### Important Websites
Create an account on the following websites if you haven't already. They are good sources of contests, challenges, problemsets, and tutorials.

1) [CodeForces](https://codeforces.com/) is a popular site that frequently hosts contests that allow users to compete for points and rankings. There 
2) [HackerRank](https://www.hackerrank.com/) is a great place to get started with competitive programming, and if you remember, it is the platform we used for the amFOSS  task.
3) [LeetCode](https://leetcode.com/) is  comprehensive platform that not only challenges you with coding problems but also helps you prepare for technical interviews.
4) [CodeChef](https://www.codechef.com/) is a platform similar to codeforces that typically contains easier problems with more frequent contests.

<hr>

### Resources

[CP handbook](https://cses.fi/book/book.pdf)<br>
[Roadmap questions with Solutions](https://drive.google.com/file/d/16Jydn250Ue7K7hOfzLxw4p5jFF4ZCqWT/view)<br>
[A2OJ Ladder](https://earthshakira.github.io/a2oj-clientside/server/Ladders.html)

We will refer to these resources throughout the curriculum, so make sure you're comfortable using them.

<hr>

## <a name="aad">Competitive Programming Beginner Roadmap</a>

|  Month  |  Description |
|---|---|
[Month 1](./month1.md) | Beginner level (Basic concepts like strings, math, sorting, etc) | 
[Month 2](./month2.md) | Intermediate level (Tougher concepts such as Dynamic Programming, as well as getting familiar with contests) | 
[Month 3](./month3.md) | Advanced Level (Try even more difficult concepts such as graph theory and build a solid understanding of how to use it ) |



### Conclusion
This quick three month curriculum is merely the beginning of your competitive programming journey. There are many more topics to cover, problems to solve, and contests to win. Our main goal is to win ICPC, but even if you don't reach that level competitive programming will help improve your DSA and problem solving abilities, which is especially valuable in a computer programmer. Don't get discouraged if you don't see immediate results, the skills you learn through competitve programming will definitely help you throughout your career. Just solve problems consistently, and never stop learning. Happy coding!

### Practice Contest

Try out our custom practice contest with questions prepared by our team with questions that were used for Praveshan to the club.<br>
[Hackerrank Link](https://www.hackerrank.com/contests/amfoss-cp)